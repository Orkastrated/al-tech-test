#!/usr/bin/env bash

set -e

. /venv/bin/activate

exec uvicorn src.panda_api.main:app --host 0.0.0.0 --port "8080"