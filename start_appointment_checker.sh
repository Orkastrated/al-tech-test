#!/usr/bin/env bash

set -e

. /venv/bin/activate

exec python3 -m src.appointment_checker.main --mode "loop"