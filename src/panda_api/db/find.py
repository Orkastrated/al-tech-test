from .client import create_client


def find_one(collection_name, query):
    db_client = create_client()
    collection = db_client[collection_name]
    result = collection.find_one(query)

    return result


def find_many(collection_name, query):
    db_client = create_client()
    collection = db_client[collection_name]
    cursor = collection.find(query, {"_id": 0})
    results = []
    for item in cursor:
        results.append(item)

    return results
