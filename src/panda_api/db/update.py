from .client import create_client


def update_item(collection_name, item_id, updates):
    db_client = create_client()
    collection = db_client[collection_name]
    collection.update_one(item_id, {"$set": updates})
