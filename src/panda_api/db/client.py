from pymongo import MongoClient
from ..env import DATABASE_URL, DATABASE_NAME


def create_client():
    client = MongoClient(DATABASE_URL, uuidRepresentation="standard")
    return client[DATABASE_NAME]
