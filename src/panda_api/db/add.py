from .client import create_client


def add_items(collection_name, items):
    db_client = create_client()
    collection = db_client[collection_name]
    collection.insert_many(items)
