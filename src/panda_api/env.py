import os

DATABASE_NAME = os.environ.get("DATABASE_NAME", "panda")
DATABASE_URL = os.environ.get("DATABASE_URL", "mongodb://root:example@localhost:27017/")
