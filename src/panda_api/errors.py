from pydantic.errors import PydanticValueError


class EmptyStrError(PydanticValueError):
    code = "empty"
    msg_template = "value cannot be empty"


class InvalidNhsNumberError(PydanticValueError):
    code = "invalid_nhs_number"
    msg_template = "nhs number must be valid"


class InvalidPostcodeError(PydanticValueError):
    code = "invalid_postcode"
    msg_template = "postcode must be valid"


class InvalidDobError(PydanticValueError):
    code = "invalid_dob"
    msg_template = "date of birth must be in the format yyyy-mm-dd"


class MinimumRequiredFieldsError(PydanticValueError):
    code = "invalid_query"
    msg_template = "At least one field must be provided"
