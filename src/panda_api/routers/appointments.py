from fastapi import APIRouter, HTTPException
import structlog
from uuid import uuid4
from http import HTTPStatus

from ..models.appointments import (
    Appointments,
    NewAppointments,
    AppointmentsToUpdate,
    UpdatedAppointments,
    AppointmentQuery,
)
from ..db import add, find, update
from ..env import DATABASE_NAME
from ..constants import (
    APPOINTMENTS_PATH_PREFIX,
    ADD_PATH,
    UPDATE_PATH,
    FIND_PATH,
    APPOINTMENTS_TAG,
    COL_APPOINTMENTS,
    ID_FIELD,
    STATUS_FIELD,
    APPOINTMENTS_FIELD,
    ERROR_FIELD,
    ERRORS_FIELD,
    ACTIVE_STATUS,
    CANCELLED_STATUS,
)

LOGGER = structlog.getLogger()
router = APIRouter(prefix=APPOINTMENTS_PATH_PREFIX, tags=[APPOINTMENTS_TAG])


@router.put(ADD_PATH, response_model=Appointments, tags=[APPOINTMENTS_TAG])
async def add_new_appointments(new_appointments: NewAppointments):
    appointments_to_add = []

    for appointment in new_appointments.appointments:
        new_appointment = appointment.dict()
        new_appointment[ID_FIELD] = uuid4()
        new_appointment[STATUS_FIELD] = ACTIVE_STATUS
        appointments_to_add.append(new_appointment)
    try:
        add.add_items(COL_APPOINTMENTS, appointments_to_add)
    except Exception as exc:
        LOGGER.error(
            "Failed to add new appointments",
            db_name=DATABASE_NAME,
            collection=COL_APPOINTMENTS,
            task="add_new_appointments",
        )
        LOGGER.error(exc)
        raise HTTPException(
            status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
            detail="Failed to add new appointments",
        )

    LOGGER.info("Successfully added new appointments", task="add_new_appointments")
    return {APPOINTMENTS_FIELD: appointments_to_add}


@router.post(
    UPDATE_PATH,
    response_model=UpdatedAppointments,
    tags=[APPOINTMENTS_TAG],
)
async def update_appointments(appointments_to_update: AppointmentsToUpdate):
    errors = []
    updated = []

    for appointment_updates in appointments_to_update.appointments:
        appointment_id = appointment_updates.id

        updates = appointment_updates.dict(exclude_none=True)
        del updates[ID_FIELD]

        try:
            current_appointment = find.find_one(
                COL_APPOINTMENTS, {ID_FIELD: appointment_id}
            )
        except Exception as exc:
            LOGGER.error(
                "Failed to find appointment",
                id=appointment_id,
                db_name=DATABASE_NAME,
                collection=COL_APPOINTMENTS,
                task="update_appointments",
            )
            LOGGER.error(exc)
            errors.append(
                {ID_FIELD: appointment_id, ERROR_FIELD: "error finding appointment"}
            )
            continue

        if not current_appointment:
            LOGGER.error(
                "Appointment does not exist",
                id=appointment_id,
                db_name=DATABASE_NAME,
                collection=COL_APPOINTMENTS,
                task="update_appointments",
            )
            errors.append(
                {ID_FIELD: appointment_id, ERROR_FIELD: "appointment does not exist"}
            )
            continue

        if current_appointment[STATUS_FIELD] == CANCELLED_STATUS:
            LOGGER.error(
                "Cancelled appointments cannot be updated",
                id=appointment_id,
                db_name=DATABASE_NAME,
                collection=COL_APPOINTMENTS,
                task="update_appointments",
            )
            errors.append(
                {
                    ID_FIELD: appointment_id,
                    ERROR_FIELD: "cancelled appointment cannot be updated",
                }
            )
            continue

        try:
            update.update_item(COL_APPOINTMENTS, {ID_FIELD: appointment_id}, updates)
        except Exception as exc:
            LOGGER.error(
                "Failed to update appointment",
                id=appointment_id,
                db_name=DATABASE_NAME,
                collection=COL_APPOINTMENTS,
                task="update_appointments",
            )
            LOGGER.error(exc)
            errors.append(
                {ID_FIELD: appointment_id, ERROR_FIELD: "error updating appointment"}
            )
            continue
        LOGGER.info(
            "Successfully updated appointment",
            id=appointment_id,
            task="update_appointments",
        )
        updated.append(current_appointment | updates)

    LOGGER.info(
        "Completed updating appointments",
        successful=f"{len(updated)}",
        errors=f"{len(errors)}",
        task="update_appointments",
    )
    return {APPOINTMENTS_FIELD: updated, ERRORS_FIELD: errors}


@router.post(FIND_PATH, response_model=Appointments, tags=[APPOINTMENTS_TAG])
async def find_appointments(query: AppointmentQuery):
    query = query.dict(exclude_none=True)
    try:
        results = find.find_many(COL_APPOINTMENTS, query)
    except Exception as exc:
        LOGGER.error(
            "Error finding appointments",
            query=query,
            db_name=DATABASE_NAME,
            collection=COL_APPOINTMENTS,
            task="find_appointments",
        )
        LOGGER.error(exc)
        raise HTTPException(
            status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
            detail="Error finding appointments",
        )
    LOGGER.info(
        "Completed finding appointments",
        task="find_appointments",
    )
    return {APPOINTMENTS_FIELD: results}
