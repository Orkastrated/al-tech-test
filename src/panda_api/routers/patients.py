from fastapi import APIRouter, HTTPException
import structlog
from http import HTTPStatus

from ..models.patients import Patients, UpdatedPatients, PatientsToUpdate, PatientQuery

from ..db import add, find, update
from ..env import DATABASE_NAME
from ..constants import (
    PATIENTS_PATH_PREFIX,
    ADD_PATH,
    UPDATE_PATH,
    FIND_PATH,
    PATIENTS_TAG,
    COL_PATIENTS,
    PATIENTS_FIELD,
    NHS_NUMBER_FIELD,
    ERROR_FIELD,
    ERRORS_FIELD,
)

LOGGER = structlog.getLogger()
router = APIRouter(prefix=PATIENTS_PATH_PREFIX, tags=[PATIENTS_TAG])


@router.put(ADD_PATH, response_model=Patients, tags=[PATIENTS_TAG])
async def add_new_patients(new_patients: Patients):
    patients_to_add = []

    for patient in new_patients.patients:
        new_patient = patient.dict()
        patients_to_add.append(new_patient)
    try:
        add.add_items(COL_PATIENTS, patients_to_add)
    except Exception as exc:
        LOGGER.error(
            "Failed to add new patients",
            db_name=DATABASE_NAME,
            collection=COL_PATIENTS,
            task="add_new_patients",
        )
        LOGGER.error(exc)
        raise HTTPException(
            status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
            detail="Failed to add new patients",
        )

    LOGGER.info("Successfully added new patients", task="add_new_patients")
    return {PATIENTS_FIELD: patients_to_add}


@router.post(
    UPDATE_PATH,
    response_model=UpdatedPatients,
    tags=[PATIENTS_TAG],
)
async def update_patients(patients_to_update: PatientsToUpdate):
    errors = []
    updated = []

    for patient_updates in patients_to_update.patients:
        nhs_number = patient_updates.nhs_number

        updates = patient_updates.dict(exclude_none=True)
        del updates[NHS_NUMBER_FIELD]

        try:
            current_patient = find.find_one(
                COL_PATIENTS, {NHS_NUMBER_FIELD: nhs_number}
            )
        except Exception as exc:
            LOGGER.error(
                "Failed to find patient",
                nhs_number=nhs_number,
                db_name=DATABASE_NAME,
                collection=COL_PATIENTS,
                task="update_patients",
            )
            LOGGER.error(exc)
            errors.append(
                {NHS_NUMBER_FIELD: nhs_number, ERROR_FIELD: "error finding patient"}
            )
            continue

        if not current_patient:
            LOGGER.error(
                "Patient does not exist",
                nhs_number=nhs_number,
                db_name=DATABASE_NAME,
                collection=COL_PATIENTS,
                task="patient_updates",
            )
            errors.append(
                {NHS_NUMBER_FIELD: nhs_number, ERROR_FIELD: "patient does not exist"}
            )
            continue

        try:
            update.update_item(COL_PATIENTS, {NHS_NUMBER_FIELD: nhs_number}, updates)
        except Exception as exc:
            LOGGER.error(
                "Failed to update patient",
                nhs_number=nhs_number,
                db_name=DATABASE_NAME,
                collection=COL_PATIENTS,
                task="update_patients",
            )
            LOGGER.error(exc)
            errors.append(
                {NHS_NUMBER_FIELD: nhs_number, ERROR_FIELD: "error updating patient"}
            )
            continue
        LOGGER.info(
            "Successfully updated patient",
            nhs_number=nhs_number,
            task="update_patients",
        )
        updated.append(current_patient | updates)

    LOGGER.info(
        "Completed updating patients",
        successful=f"{len(updated)}",
        errors=f"{len(errors)}",
        task="update_patients",
    )
    return {PATIENTS_FIELD: updated, ERRORS_FIELD: errors}


@router.post(FIND_PATH, response_model=Patients, tags=[PATIENTS_TAG])
async def find_patients(query: PatientQuery):
    query = query.dict(exclude_none=True)
    try:
        results = find.find_many(COL_PATIENTS, query)
    except Exception as exc:
        LOGGER.error(
            "Error finding patients",
            query=query,
            db_name=DATABASE_NAME,
            collection=COL_PATIENTS,
            task="find_patients",
        )
        LOGGER.error(exc)
        raise HTTPException(
            status_code=HTTPStatus.INTERNAL_SERVER_ERROR,
            detail="Error finding patients",
        )
    LOGGER.info(
        "Completed finding patients",
        task="find_patients",
    )
    return {PATIENTS_FIELD: results}
