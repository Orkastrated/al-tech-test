import structlog
from datetime import datetime, timezone


def timestamper(_, __, event_dict):
    event_dict["timestamp"] = datetime.now(timezone.utc).isoformat()
    return event_dict


def configure_logging():
    structlog.configure(
        processors=[
            structlog.processors.add_log_level,
            structlog.processors.StackInfoRenderer(),
            structlog.dev.set_exc_info,
            structlog.processors.format_exc_info,
            timestamper,
            structlog.processors.JSONRenderer(),
        ]
    )
