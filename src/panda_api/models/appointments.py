from pydantic import BaseModel, validator, UUID4, root_validator
from typing import List, Literal, Optional
from datetime import datetime, timezone
import structlog

from ..validators import nhs_number, postcode
from ..errors import (
    EmptyStrError,
    InvalidNhsNumberError,
    InvalidPostcodeError,
    MinimumRequiredFieldsError,
)
from ..constants import ACTIVE_STATUS, ATTENDED_STATUS, MISSED_STATUS, CANCELLED_STATUS

LOGGER = structlog.getLogger()


class NewAppointment(BaseModel):
    patient: str
    time: datetime
    duration: str
    clinician: str
    department: str
    postcode: str

    @validator("*")
    def check_for_empty_strings(cls, v, field):
        if field.type_ == str and v == "":
            LOGGER.error(
                "Invalid value, strings cannot be empty",
                error="Unprocessable Content",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="check_for_empty_strings",
            )
            raise EmptyStrError
        return v

    @validator("patient")
    def validate_nhs_number(cls, v):
        valid = nhs_number.validate(v)
        if not valid:
            LOGGER.error(
                "Invalid NHS Number",
                error="Unprocessable Content",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="validate_nhs_number",
            )
            raise InvalidNhsNumberError
        return v

    @validator("postcode")
    def validate_postcode(cls, v):
        valid = postcode.validate(v)
        if not valid:
            LOGGER.error(
                "Invalid Postcode",
                error="Unprocessable Content",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="validate_postcode",
            )
            raise InvalidPostcodeError
        return v

    @validator("time")
    def include_timezone(cls, v):
        if v.tzname() is None:
            return v.astimezone(timezone.utc)
        return v


class NewAppointments(BaseModel):
    appointments: List[NewAppointment]


class Appointment(NewAppointment):
    status: Literal[ACTIVE_STATUS, ATTENDED_STATUS, MISSED_STATUS, CANCELLED_STATUS]
    id: UUID4


class Appointments(BaseModel):
    appointments: List[Appointment]


class AppointmentUpdates(Appointment):
    patient: Optional[str]
    status: Optional[
        Literal[ACTIVE_STATUS, ATTENDED_STATUS, MISSED_STATUS, CANCELLED_STATUS]
    ]
    time: Optional[datetime]
    duration: Optional[str]
    clinician: Optional[str]
    department: Optional[str]
    postcode: Optional[str]


class AppointmentsToUpdate(BaseModel):
    appointments: List[AppointmentUpdates]


class AppointmentUpdateErrors(BaseModel):
    id: UUID4
    error: str


class UpdatedAppointments(BaseModel):
    appointments: List[Appointment]
    errors: List[AppointmentUpdateErrors]


class AppointmentQuery(BaseModel):
    patient: Optional[str]
    status: Optional[
        Literal[ACTIVE_STATUS, ATTENDED_STATUS, MISSED_STATUS, CANCELLED_STATUS]
    ]
    time: Optional[datetime]
    clinician: Optional[str]
    department: Optional[str]
    id: Optional[UUID4]

    @root_validator(pre=True)
    def at_least_one_field(cls, fields):
        if all(field is None for field in fields.values()):
            LOGGER.error(
                "Invalid Query",
                error="Bad Request",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="at_least_one_field",
            )
            raise MinimumRequiredFieldsError
        return fields

    @validator("*")
    def check_for_empty_strings(cls, v, field):
        if field.type_ == str and v == "":
            LOGGER.error(
                "Invalid value, strings cannot be empty",
                error="Unprocessable Content",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="check_for_empty_strings",
            )
            raise EmptyStrError
        return v

    @validator("patient")
    def validate_nhs_number(cls, v):
        valid = nhs_number.validate(v)
        if not valid:
            LOGGER.error(
                "Invalid NHS Number",
                error="Unprocessable Content",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="validate_nhs_number",
            )
            raise InvalidNhsNumberError
        return v

    @validator("time")
    def include_timezone(cls, v):
        if v.tzname() is None:
            return v.astimezone(timezone.utc)
        return v
