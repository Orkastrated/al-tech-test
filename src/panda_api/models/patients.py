from pydantic import BaseModel, validator, root_validator
from typing import List, Optional

import structlog

from ..errors import (
    EmptyStrError,
    InvalidNhsNumberError,
    InvalidPostcodeError,
    InvalidDobError,
    MinimumRequiredFieldsError,
)
from ..validators import nhs_number, postcode, dob

LOGGER = structlog.getLogger()


class Patient(BaseModel):
    nhs_number: str
    name: str
    date_of_birth: str
    postcode: str

    @validator("*")
    def check_for_empty_strings(cls, v, field):
        if field.type_ == str and v == "":
            LOGGER.error(
                "Invalid value, strings cannot be empty",
                error="Unprocessable Content",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="check_for_empty_strings",
            )
            raise EmptyStrError
        return v

    @validator("nhs_number")
    def validate_nhs_number(cls, v):
        valid = nhs_number.validate(v)
        if not valid:
            LOGGER.error(
                "Invalid NHS Number",
                error="Unprocessable Content",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="validate_nhs_number",
            )
            raise InvalidNhsNumberError
        return v

    @validator("postcode")
    def validate_postcode(cls, v):
        valid = postcode.validate(v)
        if not valid:
            LOGGER.error(
                "Invalid Postcode",
                error="Unprocessable Content",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="validate_postcode",
            )
            raise InvalidPostcodeError
        return v

    @validator("date_of_birth")
    def validate_dob(cls, v):
        valid = dob.validate(v)
        if not valid:
            LOGGER.error(
                "Invalid Date of Birth",
                error="Unprocessable Content",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="validate_dob",
            )
            raise InvalidDobError
        return v


class Patients(BaseModel):
    patients: List[Patient]


class PatientUpdates(Patient):
    name: Optional[str]
    date_of_birth: Optional[str]
    postcode: Optional[str]


class PatientsToUpdate(BaseModel):
    patients: List[PatientUpdates]


class PatientUpdateErrors(BaseModel):
    nhs_number: str
    error: str


class UpdatedPatients(BaseModel):
    patients: List[Patient]
    errors: List[PatientUpdateErrors]


class PatientQuery(Patient):
    nhs_number: Optional[str]
    name: Optional[str]
    date_of_birth: Optional[str]
    postcode: Optional[str]

    @root_validator(pre=True)
    def at_least_one_field(cls, fields):
        if all(field is None for field in fields.values()):
            LOGGER.error(
                "Invalid Query",
                error="Bad Request",
                status="Validation Error",
                model=f"{cls.__name__}",
                task="at_least_one_field",
            )
            raise MinimumRequiredFieldsError
        return fields
