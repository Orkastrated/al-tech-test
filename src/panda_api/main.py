from fastapi import FastAPI

from .json_logging import configure_logging
from .routers import patients, appointments

configure_logging()

app = FastAPI()

app.include_router(patients.router)
app.include_router(appointments.router)
