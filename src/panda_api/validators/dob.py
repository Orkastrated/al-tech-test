import re
import structlog

LOGGER = structlog.getLogger()


def validate(dob):
    dob = dob.strip()
    result = re.match(r"^\d{4}-\d{2}-\d{2}$", dob)
    if not result:
        LOGGER.error("Invalid Date of Birth")
        return False
    return True
