import structlog

LOGGER = structlog.getLogger()


def validate(nhs_number):
    if len(nhs_number) != 10:
        LOGGER.error("Invalid NHS Number, length is not 10")
        return False

    if not nhs_number.isnumeric():
        LOGGER.error("Invalid NHS Number, invalid characters")
        return False

    other_digits, check_digit = nhs_number[:-1], int(nhs_number[-1])
    weighted_digits = []

    for index, digit in enumerate(other_digits):
        digit = int(digit)
        weighted_digits.append(digit * (10 - index))

    total_of_weighted_digits = sum(weighted_digits)
    remainder = total_of_weighted_digits % 11
    check_value = 11 - remainder

    if check_value == 11:
        check_value = 0

    if check_value == 10:
        LOGGER.error("Invalid NHS Number, check value is 10")
        return False

    if check_value != check_digit:
        LOGGER.error("Invalid NHS Number, check value does not match check digit")
        return False

    return True
