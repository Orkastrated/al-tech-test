import re
import structlog

LOGGER = structlog.getLogger()


def validate(postcode):
    postcode = postcode.replace(" ", "").upper()
    result = re.match(r"^[A-Z]{1,2}\d[A-Z\d]? ?\d[A-Z]{2}$", postcode)
    if not result:
        LOGGER.error("Invalid Postcode")
        return False
    return True
