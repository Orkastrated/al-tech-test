import random
from datetime import datetime, timezone, timedelta
from uuid import uuid4
from pymongo import MongoClient

example_names = [
    "Mr Lawrence Lucas",
    "Marian Smith",
    "Mr Douglas Donnelly",
    "Mary Thomas",
    "Nicholas Rowley",
]

example_dobs = [
    "1991-02-08",
    "1976-04-14",
    "1962-05-17",
    "2011-08-25",
    "2022-10-30",
]

example_postcodes = [
    "N6 2FA",
    "WA55 8HE",
    "M4 2ST",
    "N1 5ZG",
    "TF0 9DA",
]

example_nhs_numbers = [
    "1373645350",
    "1953262716",
    "0021403597",
    "0240288238",
    "0699052556",
    "0544469364",
    "1635754941",
    "1431315257",
    "1859995799",
    "1685807151",
]

example_departments = ["oncology", "gastroentology", "orthopaedics", "paediatrics"]

example_clinicians = [
    "Jason Holloway",
    "Bethany Rice-Hammond",
    "Dr Jordan Lewis",
    "Ms Geraldine Collins",
    "Alexandra Watson",
]

example_durations = ["15m", "30m", "45m", "1h", "1h30m"]

statuses = [
    "active",
    "attended",
    "missed",
    "cancelled",
]


def generate_patients():
    print("generating patients...")
    count = 0
    patients = []

    while count < 10:
        patient = {
            "nhs_number": example_nhs_numbers[count],
            "name": random.choice(example_names),
            "date_of_birth": random.choice(example_dobs),
            "postcode": random.choice(example_postcodes),
        }
        patients.append(patient)
        count += 1

    print("generate patients complete!")
    return patients


def generate_appointments():
    print("generating appointments...")
    count = 0
    appointments = []

    while count < 10:
        appointment = {
            "patient": random.choice(example_nhs_numbers),
            "status": random.choice(statuses),
            "time": (
                datetime.now(timezone.utc) + timedelta(hours=random.randint(0, 10))
            ),
            "duration": random.choice(example_durations),
            "clinician": random.choice(example_clinicians),
            "department": random.choice(example_departments),
            "postcode": random.choice(example_postcodes),
            "id": uuid4(),
        }
        appointments.append(appointment)
        count += 1

    print("generate appointments complete!")
    return appointments


def generate_and_load_data():
    print("generating data...")
    patients = generate_patients()
    appointments = generate_appointments()

    client = MongoClient(
        "mongodb://root:example@localhost:27017/", uuidRepresentation="standard"
    )
    db_client = client["panda"]

    print("loading patients...")
    patients_col = db_client["patients"]
    try:
        patients_col.insert_many(patients)
    except Exception as exc:
        print("ERROR loading patients")
        print(exc)
        return

    print("loading appointments...")
    appointments_col = db_client["appointments"]
    try:
        appointments_col.insert_many(appointments)
    except Exception as exc:
        print("ERROR loading appointments")
        print(exc)
        return

    print("complete!")


if __name__ == "__main__":
    generate_and_load_data()
