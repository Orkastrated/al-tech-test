import os

DATABASE_NAME = os.environ.get("DATABASE_NAME", "panda")
DATABASE_URL = os.environ.get("DATABASE_URL", "mongodb://root:example@localhost:27017/")
COLLECTION_NAME = os.environ.get("COLLECTION_NAME", "appointments")
