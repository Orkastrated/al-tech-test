from pymongo import MongoClient
from ..env import DATABASE_URL, DATABASE_NAME


def create_client():
    client = MongoClient(DATABASE_URL, uuidRepresentation="standard")
    return client[DATABASE_NAME]


def find(collection_name, query):
    db_client = create_client()
    collection = db_client[collection_name]
    cursor = collection.find(query, {"_id": 0})
    results = []
    for item in cursor:
        results.append(item)

    return results


def update(collection_name, ids, updates):
    db_client = create_client()
    collection = db_client[collection_name]
    collection.update_one({"$or": ids}, {"$set": updates})
