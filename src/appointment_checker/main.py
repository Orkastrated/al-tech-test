from datetime import datetime, timezone, timedelta
import re
import argparse
import time

from .db import db
from .env import COLLECTION_NAME


def parse_duration(duration):
    duration_parts = re.findall("\d+|\D+", duration)
    no_of_duration_parts = len(duration_parts)
    if no_of_duration_parts == 4:
        return {"hours": int(duration_parts[0]), "mins": int(duration_parts[2])}

    if no_of_duration_parts == 2:
        if duration_parts[1] == "h":
            return {"hours": int(duration_parts[0]), "mins": 0}
        if duration_parts[1] == "m":
            return {"hours": 0, "mins": int(duration_parts[0])}

    return None


def check_appointments():
    print("Checking appointments...")

    now = datetime.now(timezone.utc)
    start_of_day = datetime(now.year, now.month, now.day, tzinfo=timezone.utc)
    end_of_day = start_of_day + timedelta(days=1)

    query = {"time": {"$gte": start_of_day, "$lt": end_of_day}, "status": "active"}

    print("finding today's active appointments...")
    try:
        results = db.find(COLLECTION_NAME, query)
    except Exception as exc:
        print("ERROR finding appointments")
        print(exc)
        return
    print(f"found {len(results)} active appointments")

    print("determining missed appointments")
    missed = []
    for appointment in results:
        appointment_id = appointment["id"]
        appointment_start = appointment["time"].astimezone(timezone.utc)
        duration = appointment["duration"]
        parsed_duration = parse_duration(duration)
        appointment_end = appointment_start + timedelta(
            hours=parsed_duration["hours"], minutes=parsed_duration["mins"]
        )

        if now > appointment_end:
            missed.append({"id": appointment_id})
    print(f"found {len(missed)} missed appointments")

    if len(missed) > 0:
        print("Updating the status of missed appointments...")
        try:
            db.update(COLLECTION_NAME, missed, {"status": "missed"})
        except Exception as exc:
            print("ERROR updating appointments")
            print(exc)
            return
    else:
        print("Nothing to update.")

    print("Complete...")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mode", default="run_once")
    args = parser.parse_args()
    mode = args.mode

    # For testing, single runs or running regularly via something like cron
    if mode == "run_once":
        print("Running once...")
        check_appointments()
        print("Run complete!")

    # For if you'd rather python manage running it regularly
    elif mode == "loop":
        print("Running in a loop...")
        interval_in_mins = 15
        interval = interval_in_mins * 60
        while True:
            print("Starting Run")
            check_appointments()
            print(f"Run complete, sleeping for {interval_in_mins}m...")
            time.sleep(interval)

    else:
        print("Invalid Mode, use 'run_once' or 'loop'")
