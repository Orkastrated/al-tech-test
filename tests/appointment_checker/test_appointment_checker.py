from datetime import datetime, timezone
from uuid import UUID
import copy

from src.appointment_checker.main import check_appointments

appointments = [
    {
        "clinician": "Jason Holloway",
        "department": "oncology",
        "duration": "15m",
        "id": UUID("e6c181dd-afac-4f2b-9b17-1437771159b1"),
        "patient": "1373645350",
        "postcode": "UB56 7XQ",
        "status": "active",
        "time": datetime.fromisoformat("2023-06-10T10:00:00+00:00"),
    },
    {
        "clinician": "Bethany Rice-Hammond",
        "department": "gastroentology",
        "duration": "30m",
        "id": UUID("01542f70-929f-4c9a-b4fa-e672310d7e78"),
        "patient": "1953262716",
        "postcode": "IM2N 4LG",
        "status": "active",
        "time": datetime.fromisoformat("2023-06-10T14:00:00+00:00"),
    },
    {
        "clinician": "Joseph Savage",
        "department": "orthopaedics",
        "duration": "1h30m",
        "id": UUID("c455638f-99aa-478a-82bc-fd76da4991f8"),
        "patient": "0240288238",
        "postcode": "CW6E 2ET",
        "status": "active",
        "time": datetime.fromisoformat("2023-06-10T10:15:00+00:00"),
    },
]


def test__appointment_checker__calls_db_find__when_called(mocker):
    mock_time = mocker.patch("src.appointment_checker.main.datetime")
    mock_time.now.return_value = datetime(2023, 6, 10, 12, 0, tzinfo=timezone.utc)
    mock_time.return_value = datetime(2023, 6, 10, 0, 0, tzinfo=timezone.utc)
    mock_find = mocker.patch("src.appointment_checker.db.db.find")
    mock_find.return_value = appointments
    mocker.patch("src.appointment_checker.db.db.update")

    check_appointments()
    assert mock_find.call_count == 1
    mock_find.assert_has_calls(
        [
            mocker.call(
                "appointments",
                {
                    "time": {
                        "$gte": datetime(2023, 6, 10, 0, 0, tzinfo=timezone.utc),
                        "$lt": datetime(2023, 6, 11, 0, 0, tzinfo=timezone.utc),
                    },
                    "status": "active",
                },
            )
        ]
    )


def test_appointment_checker__calls_db_update_only_for_missed_appointments__when_missed_appointments_have_been_found(
    mocker,
):
    mock_time = mocker.patch("src.appointment_checker.main.datetime")
    mock_time.now.return_value = datetime(2023, 6, 10, 12, 0, tzinfo=timezone.utc)
    mock_time.return_value = datetime(2023, 6, 10, 0, 0, tzinfo=timezone.utc)
    mock_find = mocker.patch("src.appointment_checker.db.db.find")
    mock_find.return_value = appointments
    mock_update = mocker.patch("src.appointment_checker.db.db.update")

    check_appointments()
    assert mock_update.call_count == 1
    mock_update.assert_has_calls(
        [
            mocker.call(
                "appointments",
                [
                    {"id": UUID("e6c181dd-afac-4f2b-9b17-1437771159b1")},
                    {"id": UUID("c455638f-99aa-478a-82bc-fd76da4991f8")},
                ],
                {"status": "missed"},
            )
        ]
    )


def test_appointment_checker__does_not_call_db_update_many__when_no_appointments_have_been_missed(
    mocker,
):
    mock_time = mocker.patch("src.appointment_checker.main.datetime")
    mock_time.now.return_value = datetime(2023, 6, 10, 12, 0, tzinfo=timezone.utc)
    mock_time.return_value = datetime(2023, 6, 10, 0, 0, tzinfo=timezone.utc)
    mock_find = mocker.patch("src.appointment_checker.db.db.find")
    all_active_appointments = copy.deepcopy(appointments)
    all_active_appointments[0]["time"] = datetime.fromisoformat(
        "2023-06-10T20:00:00+00:00"
    )
    all_active_appointments[2]["time"] = datetime.fromisoformat(
        "2023-06-10T21:00:00+00:00"
    )
    mock_find.return_value = all_active_appointments
    mock_update = mocker.patch("src.appointment_checker.db.db.update")

    check_appointments()
    mock_update.assert_not_called()
