mock_db_appointment = {
    "patient": "1953262716",
    "time": "2023-06-07T21:00:00+00:00",
    "duration": "15m",
    "clinician": "Jason Holloway",
    "department": "oncology",
    "postcode": "UB56 7XQ",
    "status": "attended",
    "id": "e6c181dd-afac-4f2b-9b17-1437771159b1",
}


# Happy Path
def test__find_appointments__calls_db_find__when_called_with_valid_query(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_db_appointment]
    mock_query = {"clinician": "Jason Holloway"}
    test_client.post("/appointments/find", json=mock_query)

    mock_find_many.assert_called()
    mock_find_many.assert_called_with("appointments", {"clinician": "Jason Holloway"})


def test__find_appointments__returns_appointments__when_called_with_valid_query(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_db_appointment]
    mock_query = {"clinician": "Jason Holloway"}
    res = test_client.post("/appointments/find", json=mock_query)

    assert res.status_code == 200
    assert res.json() == {
        "appointments": [
            {
                "clinician": "Jason Holloway",
                "department": "oncology",
                "duration": "15m",
                "id": "e6c181dd-afac-4f2b-9b17-1437771159b1",
                "patient": "1953262716",
                "postcode": "UB56 7XQ",
                "status": "attended",
                "time": "2023-06-07T21:00:00+00:00",
            }
        ]
    }


# Unhappy Path - Request validation errors
def test__find_appointments__returns_422__when_nhs_number_is_invalid(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_db_appointment]
    mock_query = {"patient": "1234567882"}
    res = test_client.post("/appointments/find", json=mock_query)

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patient"],
                "msg": "nhs number must be valid",
                "type": "value_error.invalid_nhs_number",
            }
        ]
    }


def test__find_appointments__returns_422__when_status_is_not_one_of_the_allowed_values(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_db_appointment]
    mock_query = {"status": "other"}
    res = test_client.post("/appointments/find", json=mock_query)

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "ctx": {
                    "given": "other",
                    "permitted": ["active", "attended", "missed", "cancelled"],
                },
                "loc": ["body", "status"],
                "msg": "unexpected value; permitted: 'active', 'attended', "
                "'missed', 'cancelled'",
                "type": "value_error.const",
            }
        ]
    }


def test__find_appointments__returns_422__when_time_is_not_a_valid_datetime(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_db_appointment]
    mock_query = {"time": "2023-06-08"}
    res = test_client.post("/appointments/find", json=mock_query)

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "time"],
                "msg": "invalid datetime format",
                "type": "value_error.datetime",
            }
        ]
    }


def test__find_appointments__returns_422__when_query_has_no_fields(test_client, mocker):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_db_appointment]
    mock_query = {}
    res = test_client.post("/appointments/find", json=mock_query)

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "__root__"],
                "msg": "At least one field must be provided",
                "type": "value_error.invalid_query",
            }
        ]
    }


# Unhappy Path - Error handling
def test__find_appointments__returns_500__when_there_is_an_error_finding_appointments(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.side_effect = RuntimeError("mock exception")
    mock_query = {"status": "active"}
    res = test_client.post("/appointments/find", json=mock_query)

    assert res.status_code == 500
    assert res.json() == {"detail": "Error finding appointments"}
