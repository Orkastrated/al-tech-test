mock_valid_patient_a = {
    "nhs_number": "1953262716",
    "name": "Jason Holloway",
    "date_of_birth": "1980-02-15",
    "postcode": "UB56 7XQ",
}

mock_valid_patient_b = {
    "nhs_number": "1373645350",
    "name": "Bethany Rice-Hammond",
    "date_of_birth": "1997-10-21",
    "postcode": "IM2N 4LG",
}


# Happy path
def test__update_patients__calls_db_find_for_each_updated_patient__when_called_with_valid_updates(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.side_effect = [mock_valid_patient_a, mock_valid_patient_b]
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [
        {"nhs_number": "1953262716", "postcode": "N1 5ZG"},
        {"nhs_number": "1373645350", "postcode": "TF0 9DA"},
    ]
    test_client.post("/patients/update", json={"patients": mock_updates})

    assert mock_find_one.call_count == 2
    mock_find_one.assert_has_calls(
        [
            mocker.call("patients", {"nhs_number": "1953262716"}),
            mocker.call("patients", {"nhs_number": "1373645350"}),
        ]
    )


def test__update_patients__calls_db_update_for_each_updated_patient__when_called_with_valid_updates(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.side_effect = [mock_valid_patient_a, mock_valid_patient_b]
    mock_update_item = mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [
        {"nhs_number": "1953262716", "postcode": "N1 5ZG"},
        {"nhs_number": "1373645350", "postcode": "TF0 9DA"},
    ]
    test_client.post("/patients/update", json={"patients": mock_updates})

    assert mock_update_item.call_count == 2
    mock_update_item.assert_has_calls(
        [
            mocker.call(
                "patients",
                {"nhs_number": "1953262716"},
                {"postcode": "N1 5ZG"},
            ),
            mocker.call(
                "patients",
                {"nhs_number": "1373645350"},
                {"postcode": "TF0 9DA"},
            ),
        ]
    )


def test__update_patients__returns_200_and_updated_patients__when_called_with_valid_updates(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.side_effect = [mock_valid_patient_a, mock_valid_patient_b]
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [
        {"nhs_number": "1953262716", "postcode": "N1 5ZG"},
        {"nhs_number": "1373645350", "postcode": "TF0 9DA"},
    ]
    res = test_client.post("/patients/update", json={"patients": mock_updates})

    assert res.status_code == 200
    assert res.json() == {
        "patients": [
            {
                "nhs_number": "1953262716",
                "name": "Jason Holloway",
                "date_of_birth": "1980-02-15",
                "postcode": "N1 5ZG",
            },
            {
                "nhs_number": "1373645350",
                "name": "Bethany Rice-Hammond",
                "date_of_birth": "1997-10-21",
                "postcode": "TF0 9DA",
            },
        ],
        "errors": [],
    }


# Unhappy path - Request validation failures
def test__update_patients__returns_422__nhs_number_field_is_missing(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"postcode": "N1 5ZG"}]
    res = test_client.post("/patients/update", json={"patients": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "nhs_number"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__update_patients__returns_422__nhs_number_field_is_empty(test_client, mocker):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"nhs_number": "", "postcode": "N1 5ZG"}]
    res = test_client.post("/patients/update", json={"patients": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "nhs_number"],
                "msg": "nhs number must be valid",
                "type": "value_error.invalid_nhs_number",
            }
        ]
    }


def test__update_patients__returns_422__nhs_number_field_contains_an_invalid_nhs_number(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"nhs_number": "1234567882", "postcode": "N1 5ZG"}]
    res = test_client.post("/patients/update", json={"patients": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "nhs_number"],
                "msg": "nhs number must be valid",
                "type": "value_error.invalid_nhs_number",
            }
        ]
    }


def test__update_patients__returns_422__when_dob_field_contains_an_invalid_dob(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"nhs_number": "1953262716", "date_of_birth": "198-01-1B"}]
    res = test_client.post("/patients/update", json={"patients": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "date_of_birth"],
                "msg": "date of birth must be in the format yyyy-mm-dd",
                "type": "value_error.invalid_dob",
            }
        ]
    }


def test__update_patients__returns_422__when_postcode_field_contains_an_invalid_postcode(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"nhs_number": "1953262716", "postcode": "UB 7XQ"}]
    res = test_client.post("/patients/update", json={"patients": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "postcode"],
                "msg": "postcode must be valid",
                "type": "value_error.invalid_postcode",
            }
        ]
    }


# Unhappy path - Error handling
def test__update_patients__returns_errors__when_there_is_an_error_finding_a_patient(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.side_effect = RuntimeError("mock exception")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"nhs_number": "1953262716", "postcode": "N1 5ZG"}]
    res = test_client.post("/patients/update", json={"patients": mock_updates})

    assert res.status_code == 200
    assert res.json() == {
        "patients": [],
        "errors": [
            {
                "error": "error finding patient",
                "nhs_number": "1953262716",
            }
        ],
    }


def test__update_patients__returns_errors__when_a_patient_does_not_exist(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.return_value = None
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"nhs_number": "1953262716", "postcode": "N1 5ZG"}]
    res = test_client.post("/patients/update", json={"patients": mock_updates})

    assert res.status_code == 200
    assert res.json() == {
        "patients": [],
        "errors": [
            {
                "error": "patient does not exist",
                "nhs_number": "1953262716",
            }
        ],
    }


def test__update_patients__returns_errors__when_there_is_an_error_updating_a_patient(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.return_value = mock_valid_patient_a
    mock_update_item = mocker.patch("src.panda_api.db.update.update_item")
    mock_update_item.side_effect = RuntimeError("mock exception")
    mock_updates = [{"nhs_number": "1953262716", "postcode": "N1 5ZG"}]
    res = test_client.post("/patients/update", json={"patients": mock_updates})

    assert res.status_code == 200
    assert res.json() == {
        "patients": [],
        "errors": [
            {
                "error": "error updating patient",
                "nhs_number": "1953262716",
            }
        ],
    }
