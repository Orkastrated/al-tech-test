import copy
import datetime
from uuid import UUID


mock_valid_new_appointment = {
    "patient": "1953262716",
    "time": "2023-06-08T14:49:47.975420+00:00",
    "duration": "15m",
    "clinician": "Jason Holloway",
    "department": "oncology",
    "postcode": "UB56 7XQ",
}


# Happy path
def test__add_new_appointments__calls_db_add_items__when_called_with_valid_new_appointments(
    test_client, mocker
):
    mock_add_items = mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")
    test_client.put(
        "/appointments/add", json={"appointments": [mock_valid_new_appointment]}
    )

    mock_add_items.assert_called()
    mock_add_items.assert_called_with(
        "appointments",
        [
            {
                "patient": "1953262716",
                "time": datetime.datetime(
                    2023, 6, 8, 14, 49, 47, 975420, tzinfo=datetime.timezone.utc
                ),
                "duration": "15m",
                "clinician": "Jason Holloway",
                "department": "oncology",
                "postcode": "UB56 7XQ",
                "id": UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6"),
                "status": "active",
            }
        ],
    )


def test__add_new_appointments__returns_200_added_appointments__when_called_with_valid_new_appointments(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    res = test_client.put(
        "/appointments/add", json={"appointments": [mock_valid_new_appointment]}
    )
    assert res.status_code == 200
    assert res.json() == {
        "appointments": [
            {
                "patient": "1953262716",
                "time": "2023-06-08T14:49:47.975420+00:00",
                "duration": "15m",
                "clinician": "Jason Holloway",
                "department": "oncology",
                "postcode": "UB56 7XQ",
                "id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6",
                "status": "active",
            }
        ]
    }


# Unhappy path - Request validation failures
def test__add_new_appointments__returns_422__when_patient_field_is_missing(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    del invalid_data["patient"]

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "patient"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_patient_field_is_empty(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    invalid_data["patient"] = ""

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "patient"],
                "msg": "nhs number must be valid",
                "type": "value_error.invalid_nhs_number",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_patient_field_contains_an_invalid_nhs_number(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")
    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    invalid_data["patient"] = "1234567882"

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "patient"],
                "msg": "nhs number must be valid",
                "type": "value_error.invalid_nhs_number",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_time_field_is_missing(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    del invalid_data["time"]

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "time"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_time_field_is_empty(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    invalid_data["time"] = ""

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "time"],
                "msg": "invalid datetime format",
                "type": "value_error.datetime",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_time_field_is_not_a_valid_datetime(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    invalid_data["time"] = "2023-06-08"

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "time"],
                "msg": "invalid datetime format",
                "type": "value_error.datetime",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_clinician_field_is_missing(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")
    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    del invalid_data["clinician"]

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "clinician"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_clinician_field_is_empty(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    invalid_data["clinician"] = ""

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "clinician"],
                "msg": "value cannot be empty",
                "type": "value_error.empty",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_department_field_is_missing(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    del invalid_data["department"]

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "department"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_department_field_is_empty(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    invalid_data["department"] = ""

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "department"],
                "msg": "value cannot be empty",
                "type": "value_error.empty",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_postcode_field_is_missing(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    del invalid_data["postcode"]

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "postcode"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_postcode_field_is_empty(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    invalid_data["postcode"] = ""

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "postcode"],
                "msg": "postcode must be valid",
                "type": "value_error.invalid_postcode",
            }
        ]
    }


def test__add_new_appointments__returns_422__when_postcode_field_contains_an_invalid_postcode(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")

    invalid_data = copy.deepcopy(mock_valid_new_appointment)
    invalid_data["postcode"] = "UB 7XQ"

    res = test_client.put("/appointments/add", json={"appointments": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "postcode"],
                "msg": "postcode must be valid",
                "type": "value_error.invalid_postcode",
            }
        ]
    }


# Unhappy path - Error handling
def test__add_new_appointments__returns_500__when_there_is_an_error_adding_new_appointments(
    test_client, mocker
):
    mock_add_items = mocker.patch("src.panda_api.db.add.add_items")
    mock_uuid = mocker.patch("src.panda_api.routers.appointments.uuid4")
    mock_uuid.return_value = UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")
    mock_add_items.side_effect = RuntimeError("mock exception")

    res = test_client.put(
        "/appointments/add", json={"appointments": [mock_valid_new_appointment]}
    )
    assert res.status_code == 500
    assert res.json() == {"detail": "Failed to add new appointments"}
