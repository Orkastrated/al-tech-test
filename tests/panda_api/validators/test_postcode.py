from src.panda_api.validators import postcode


def test__validate__returns_true__when_postcode_is_valid():
    valid = postcode.validate("UB56 7XQ")
    assert valid is True


def test__validate__returns_true__when_postcode_is_valid_but_lowercase():
    valid = postcode.validate("ub56 7xq")
    assert valid is True


def test__validate__returns_true__when_postcode_is_valid_but_has_no_middle_space():
    valid = postcode.validate("UB567XQ")
    assert valid is True


def test__validate__returns_true__when_postcode_is_valid_but_has_leading_spaces():
    valid = postcode.validate("  UB56 7XQ")
    assert valid is True


def test__validate__returns_true__when_postcode_is_valid_but_has_trailing_spaces():
    valid = postcode.validate("UB56 7XQ  ")
    assert valid is True


def test__validate__returns_true__when_postcode_is_valid_but_has_extra_middle_spaces():
    valid = postcode.validate("UB56  7XQ")
    assert valid is True


def test__validate__returns_false__when_postcode_outward_code_does_not_start_with_a_letter():
    valid = postcode.validate("56 7XQ")
    assert valid is False


def test__validate__returns_false__when_postcode_outward_code_is_too_short():
    valid = postcode.validate("U 7XQ")
    assert valid is False


def test__validate__returns_false__when_postcode_outward_code_is_too_long():
    valid = postcode.validate("UB567 7XQ")
    assert valid is False


def test__validate__returns_false__when_postcode_inward_code_is_too_short():
    valid = postcode.validate("UB56 7X")
    assert valid is False


def test__validate__returns_false__when_postcode_inward_code_is_too_long():
    valid = postcode.validate("UB56 7XQE")
    assert valid is False


def test__validate__returns_false__when_postcode_inward_code_does_not_start_with_a_number():
    valid = postcode.validate("UB56 DXQ")
    assert valid is False
