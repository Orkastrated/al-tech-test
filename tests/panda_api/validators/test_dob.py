from src.panda_api.validators import dob


def test__validate__returns_true__when_dob_is_valid():
    valid = dob.validate("1980-02-15")
    assert valid is True


def test__validate__returns_false__when_dob_contains_letters():
    valid = dob.validate("198A-02-1C")
    assert valid is False


def test__validate__returns_false__when_dob_deliminator_is_not_a_hyphen():
    valid = dob.validate("1980_02_15")
    assert valid is False


def test__validate__returns_false__when_year_has_too_many_digits():
    valid = dob.validate("19801-02-15")
    assert valid is False


def test__validate__returns_false__when_year_has_too_few_digits():
    valid = dob.validate("198-02-15")
    assert valid is False


def test__validate__returns_false__when_month_has_too_many_digits():
    valid = dob.validate("1980-023-15")
    assert valid is False


def test__validate__returns_false__when_month_has_too_few_digits():
    valid = dob.validate("1980-0-15")
    assert valid is False


def test__validate__returns_false__when_day_has_too_many_digits():
    valid = dob.validate("1980-02-151")
    assert valid is False


def test__validate__returns_false__when_day_has_too_few_digits():
    valid = dob.validate("1980-02-1")
    assert valid is False


def test__validate__returns_false__when_dob_not_in_the_correct_format():
    valid = dob.validate("15-02-1980")
    assert valid is False
