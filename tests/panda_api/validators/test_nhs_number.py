from src.panda_api.validators import nhs_number


def test__validate__returns_true__when_nhs_number_is_valid():
    valid = nhs_number.validate("1953262716")

    assert valid is True


def test__validate__returns_true__when_nhs_number_is_valid_and_check_value_is_11():
    valid = nhs_number.validate("1373645350")

    assert valid is True


def test__validate__returns_false__when_nhs_number_is_less_than_10_digits():
    valid = nhs_number.validate("195326271")

    assert valid is False


def test__validate__returns_false__when_nhs_number_is_more_than_10_digits():
    valid = nhs_number.validate("19532627161")

    assert valid is False


def test__validate__returns_false__when_nhs_number_is_not_numeric():
    valid = nhs_number.validate("1B53C6271D")

    assert valid is False


def test__validate__returns_false__when_nhs_number_check_value_is_10():
    valid = nhs_number.validate("1234567890")

    assert valid is False


def test__validate__returns_false__when_nhs_number_check_value_does_not_equal_check_digit():
    valid = nhs_number.validate("1234567882")

    assert valid is False
