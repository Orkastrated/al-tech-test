from uuid import UUID
import copy

mock_db_appointment_a = {
    "patient": "1953262716",
    "time": "2023-06-07T21:00:00+00:00",
    "duration": "15m",
    "clinician": "Jason Holloway",
    "department": "oncology",
    "postcode": "UB56 7XQ",
    "status": "attended",
    "id": "e6c181dd-afac-4f2b-9b17-1437771159b1",
}

mock_db_appointment_b = {
    "patient": "1373645350",
    "time": "2023-06-07T22:00:00+00:00",
    "duration": "15m",
    "clinician": "Bethany Rice-Hammond",
    "department": "oncology",
    "postcode": "IM2N 4LG",
    "status": "attended",
    "id": "e6c181dd-afac-4f2b-9b17-1437771159b1",
}


# Happy path
def test__update_appointments__calls_db_find_for_each_updated_appointment__when_called_with_valid_updates(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.side_effect = [mock_db_appointment_a, mock_db_appointment_b]
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [
        {"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "status": "attended"},
        {"id": "e6c181dd-afac-4f2b-9b17-1437771159b1", "status": "attended"},
    ]
    test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert mock_find_one.call_count == 2
    mock_find_one.assert_has_calls(
        [
            mocker.call(
                "appointments", {"id": UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")}
            ),
            mocker.call(
                "appointments", {"id": UUID("e6c181dd-afac-4f2b-9b17-1437771159b1")}
            ),
        ]
    )


def test__update_appointments__calls_db_update_for_each_updated_appointment__when_called_with_valid_updates(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.side_effect = [mock_db_appointment_a, mock_db_appointment_b]
    mock_update_item = mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [
        {"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "status": "attended"},
        {"id": "e6c181dd-afac-4f2b-9b17-1437771159b1", "status": "attended"},
    ]
    test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert mock_update_item.call_count == 2
    mock_update_item.assert_has_calls(
        [
            mocker.call(
                "appointments",
                {"id": UUID("21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6")},
                {"status": "attended"},
            ),
            mocker.call(
                "appointments",
                {"id": UUID("e6c181dd-afac-4f2b-9b17-1437771159b1")},
                {"status": "attended"},
            ),
        ]
    )


def test__update_appointments__returns_200_and_updated_appointments__when_called_with_valid_updates(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.side_effect = [mock_db_appointment_a, mock_db_appointment_b]
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [
        {"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "status": "attended"},
        {"id": "e6c181dd-afac-4f2b-9b17-1437771159b1", "status": "attended"},
    ]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 200
    assert res.json() == {
        "appointments": [
            {
                "clinician": "Jason Holloway",
                "department": "oncology",
                "duration": "15m",
                "id": "e6c181dd-afac-4f2b-9b17-1437771159b1",
                "patient": "1953262716",
                "postcode": "UB56 7XQ",
                "status": "attended",
                "time": "2023-06-07T21:00:00+00:00",
            },
            {
                "clinician": "Bethany Rice-Hammond",
                "department": "oncology",
                "duration": "15m",
                "id": "e6c181dd-afac-4f2b-9b17-1437771159b1",
                "patient": "1373645350",
                "postcode": "IM2N 4LG",
                "status": "attended",
                "time": "2023-06-07T22:00:00+00:00",
            },
        ],
        "errors": [],
    }


# Unhappy path - Request validation failures
def test__update_appointments__returns_422__when_id_field_is_missing(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"status": "attended"}]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "id"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__update_appointments__returns_422__when_id_field_is_empty(test_client, mocker):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"id": "", "status": "attended"}]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "id"],
                "msg": "value is not a valid uuid",
                "type": "type_error.uuid",
            }
        ]
    }


def test__update_appointments__returns_422__when_nhs_number_is_invalid(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [
        {"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "patient": "1234567882"}
    ]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "patient"],
                "msg": "nhs number must be valid",
                "type": "value_error.invalid_nhs_number",
            }
        ]
    }


def test__update_appointments__returns_422__when_postcode_is_invalid(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [
        {"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "postcode": "UB 7XQ"}
    ]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "postcode"],
                "msg": "postcode must be valid",
                "type": "value_error.invalid_postcode",
            }
        ]
    }


def test__update_appointments__returns_422__when_status_is_not_one_of_the_allowed_values(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "status": "other"}]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "ctx": {
                    "given": "other",
                    "permitted": ["active", "attended", "missed", "cancelled"],
                },
                "loc": ["body", "appointments", 0, "status"],
                "msg": "unexpected value; permitted: 'active', 'attended', "
                "'missed', 'cancelled'",
                "type": "value_error.const",
            }
        ]
    }


def test__update_appointments__returns_422__when_time_is_not_a_valid_datetime(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.find.find_one")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [
        {"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "time": "2023-06-08"}
    ]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "appointments", 0, "time"],
                "msg": "invalid datetime format",
                "type": "value_error.datetime",
            }
        ]
    }


# Unhappy path - Invalid updates
def test__update_appointments__returns_errors__when_attempting_to_update_a_cancelled_appointment(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_db_appointment = copy.deepcopy(mock_db_appointment_a)
    mock_db_appointment["status"] = "cancelled"
    mock_find_one.return_value = mock_db_appointment
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "status": "active"}]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 200
    assert res.json() == {
        "appointments": [],
        "errors": [
            {
                "error": "cancelled appointment cannot be updated",
                "id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6",
            }
        ],
    }


# Unhappy path - Error handling
def test__update_appointments__returns_errors__when_there_is_an_error_finding_an_appointment(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.side_effect = RuntimeError("mock exception")
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "status": "active"}]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 200
    assert res.json() == {
        "appointments": [],
        "errors": [
            {
                "error": "error finding appointment",
                "id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6",
            }
        ],
    }


def test__update_appointments__returns_errors__when_an_appointment_does_not_exist(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.return_value = None
    mocker.patch("src.panda_api.db.update.update_item")
    mock_updates = [{"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "status": "active"}]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 200
    assert res.json() == {
        "appointments": [],
        "errors": [
            {
                "error": "appointment does not exist",
                "id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6",
            }
        ],
    }


def test__update_appointments__returns_errors__when_there_is_an_error_updating_an_appointment(
    test_client, mocker
):
    mock_find_one = mocker.patch("src.panda_api.db.find.find_one")
    mock_find_one.return_value = mock_db_appointment_a
    mock_update_item = mocker.patch("src.panda_api.db.update.update_item")
    mock_update_item.side_effect = RuntimeError("mock exception")
    mock_updates = [{"id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6", "status": "active"}]
    res = test_client.post("/appointments/update", json={"appointments": mock_updates})

    assert res.status_code == 200
    assert res.json() == {
        "appointments": [],
        "errors": [
            {
                "error": "error updating appointment",
                "id": "21d1b2f9-6b66-4ab4-a2b0-59061e05e4c6",
            }
        ],
    }
