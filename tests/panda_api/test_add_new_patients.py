import copy


mock_valid_patient = {
    "nhs_number": "1953262716",
    "name": "Jason Holloway",
    "date_of_birth": "1980-02-15",
    "postcode": "UB56 7XQ",
}


# Happy path
def test__add_new_patients__calls_db_add_items__when_called_with_valid_new_patient(
    test_client, mocker
):
    mock_add_items = mocker.patch("src.panda_api.db.add.add_items")
    test_client.put("/patients/add", json={"patients": [mock_valid_patient]})

    mock_add_items.assert_called()
    mock_add_items.assert_called_with(
        "patients",
        [
            {
                "nhs_number": "1953262716",
                "name": "Jason Holloway",
                "date_of_birth": "1980-02-15",
                "postcode": "UB56 7XQ",
            }
        ],
    )


def test__add_new_patients__returns_200_added_patients__when_called_with_valid_new_patient(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")

    res = test_client.put("/patients/add", json={"patients": [mock_valid_patient]})
    assert res.status_code == 200
    assert res.json() == {
        "patients": [
            {
                "nhs_number": "1953262716",
                "name": "Jason Holloway",
                "date_of_birth": "1980-02-15",
                "postcode": "UB56 7XQ",
            }
        ]
    }


# Unhappy path - Request validation failures
def test__add_new_patient__returns_422__when_nhs_number_field_is_missing(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    del invalid_data["nhs_number"]

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "nhs_number"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__add_new_patient__returns_422__when_nhs_number_field_is_empty(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    invalid_data["nhs_number"] = ""

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "nhs_number"],
                "msg": "nhs number must be valid",
                "type": "value_error.invalid_nhs_number",
            }
        ]
    }


def test__add_new_patient__returns_422__when_nhs_number_field_contains_an_invalid_nhs_number(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    invalid_data["nhs_number"] = "1234567882"

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "nhs_number"],
                "msg": "nhs number must be valid",
                "type": "value_error.invalid_nhs_number",
            }
        ]
    }


def test__add_new_patient__returns_422__when_name_field_is_missing(test_client, mocker):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    del invalid_data["name"]

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "name"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__add_new_patient__returns_422__when_name_field_is_empty(test_client, mocker):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    invalid_data["name"] = ""

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "name"],
                "msg": "value cannot be empty",
                "type": "value_error.empty",
            }
        ]
    }


def test__add_new_patient__returns_422__when_dob_field_is_missing(test_client, mocker):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    del invalid_data["date_of_birth"]

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "date_of_birth"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__add_new_patient__returns_422__when_dob_field_is_empty(test_client, mocker):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    invalid_data["date_of_birth"] = ""

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "date_of_birth"],
                "msg": "date of birth must be in the format yyyy-mm-dd",
                "type": "value_error.invalid_dob",
            }
        ]
    }


def test__add_new_patient__returns_422__when_dob_field_contains_an_invalid_dob(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    invalid_data["date_of_birth"] = "198-01-1B"

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "date_of_birth"],
                "msg": "date of birth must be in the format yyyy-mm-dd",
                "type": "value_error.invalid_dob",
            }
        ]
    }


def test__add_new_patient__returns_422__when_postcode_field_is_missing(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    del invalid_data["postcode"]

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "postcode"],
                "msg": "field required",
                "type": "value_error.missing",
            }
        ]
    }


def test__add_new_patient__returns_422__when_postcode_field_is_empty(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    invalid_data["postcode"] = ""

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "postcode"],
                "msg": "postcode must be valid",
                "type": "value_error.invalid_postcode",
            }
        ]
    }


def test__add_new_patient__returns_422__when_postcode_field_contains_an_invalid_postcode(
    test_client, mocker
):
    mocker.patch("src.panda_api.db.add.add_items")

    invalid_data = copy.deepcopy(mock_valid_patient)
    invalid_data["postcode"] = "UB 7XQ"

    res = test_client.put("/patients/add", json={"patients": [invalid_data]})
    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "patients", 0, "postcode"],
                "msg": "postcode must be valid",
                "type": "value_error.invalid_postcode",
            }
        ]
    }


# Unhappy path - Error handling
def test__add_new_patient__returns_500__when_there_is_an_error_adding_new_patients(
    test_client, mocker
):
    mock_add_items = mocker.patch("src.panda_api.db.add.add_items")
    mock_add_items.side_effect = RuntimeError("mock exception")

    res = test_client.put("/patients/add", json={"patients": [mock_valid_patient]})
    assert res.status_code == 500
    assert res.json() == {"detail": "Failed to add new patients" ""}
