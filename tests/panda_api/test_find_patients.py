mock_valid_patient = {
    "nhs_number": "1953262716",
    "name": "Jason Holloway",
    "date_of_birth": "1980-02-15",
    "postcode": "UB56 7XQ",
}


# Happy Path
def test__find_patients__calls_db_find__when_called_with_valid_query(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_valid_patient]
    mock_query = {"nhs_number": "1953262716"}
    test_client.post("/patients/find", json=mock_query)

    mock_find_many.assert_called()
    mock_find_many.assert_called_with("patients", {"nhs_number": "1953262716"})


def test__find_patients__returns_patients__when_called_with_valid_query(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_valid_patient]
    mock_query = {"nhs_number": "1953262716"}
    res = test_client.post("/patients/find", json=mock_query)

    assert res.status_code == 200
    assert res.json() == {
        "patients": [
            {
                "nhs_number": "1953262716",
                "name": "Jason Holloway",
                "date_of_birth": "1980-02-15",
                "postcode": "UB56 7XQ",
            }
        ]
    }


# Unhappy Path - Request validation errors
def test__find_patients__returns_422__when_nhs_number_is_invalid(test_client, mocker):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_valid_patient]
    mock_query = {"nhs_number": "1234567882"}
    res = test_client.post("/patients/find", json=mock_query)

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "nhs_number"],
                "msg": "nhs number must be valid",
                "type": "value_error.invalid_nhs_number",
            }
        ]
    }


def test__find_patients__returns_422__when_dob_field_contains_an_invalid_dob(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_valid_patient]
    mock_query = {"date_of_birth": "198-01-1B"}
    res = test_client.post("/patients/find", json=mock_query)

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "date_of_birth"],
                "msg": "date of birth must be in the format yyyy-mm-dd",
                "type": "value_error.invalid_dob",
            }
        ]
    }


def test__find_patients__returns_422__when_postcode_field_contains_an_invalid_postcode(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.return_value = [mock_valid_patient]
    mock_query = {"postcode": "UB 7XQ"}
    res = test_client.post("/patients/find", json=mock_query)

    assert res.status_code == 422
    assert res.json() == {
        "detail": [
            {
                "loc": ["body", "postcode"],
                "msg": "postcode must be valid",
                "type": "value_error.invalid_postcode",
            }
        ]
    }


# Unhappy Path - Error handling
def test__find_patients__returns_500__when_there_is_an_error_finding_patients(
    test_client, mocker
):
    mock_find_many = mocker.patch("src.panda_api.db.find.find_many")
    mock_find_many.side_effect = RuntimeError("mock exception")
    mock_query = {"nhs_number": "1953262716"}
    res = test_client.post("/patients/find", json=mock_query)

    assert res.status_code == 500
    assert res.json() == {"detail": "Error finding patients"}
