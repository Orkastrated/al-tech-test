import pytest
from fastapi.testclient import TestClient

from src.panda_api.main import app


@pytest.fixture()
def test_client():
    client = TestClient(app)
    yield client
